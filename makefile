CROSS_COMPILE=

ifeq ($(CROSS_COMPILE),)

ifeq ($(LINUXHOME_4_9),)
LINUXHOME_4_9=./linux-4.9/x86
endif

OPTFLAGS=-O0

CPPFLAGS=\
        -nostdinc \
        -isystem ./compiler/include \
        -I./instrumentation \
        -I$(LINUXHOME_4_9)/arch/x86/include \
        -I$(LINUXHOME_4_9)/include \
        -I$(LINUXHOME_4_9)/arch/x86/include/uapi \
        -I$(LINUXHOME_4_9)/include/uapi \
        -I$(LINUXHOME_4_9)/arch/x86/include/generated/uapi \
        -I$(LINUXHOME_4_9)/arch/x86/include/generated \
        -I./generated/x86/include \
        -I./generated/x86/arch/x86/include/generated \
        -I./generated/x86/arch/x86/include/generated/uapi \
        -include $(LINUXHOME_4_9)/include/linux/kconfig.h \
        -D__KERNEL__ \
        -DCONFIG_OF \
        $(EXTRA_CPPFLAGS)
      

CFLAGS=\
      $(OPTFLAGS) \
      -g \
      -Wall \
      -std=gnu89
else

ifeq ($(LINUXHOME_4_9),)
LINUXHOME_4_9=./linux-4.9/arm
endif

CPPFLAGS=\
        -nostdinc \
        -isystem ./compiler/include \
        -I./instrumentation \
        -I$(LINUXHOME_4_9)/arch/arm/include \
        -I$(LINUXHOME_4_9)/include \
        -I$(LINUXHOME_4_9)/arch/arm/include/uapi \
        -I$(LINUXHOME_4_9)/include/uapi \
        -I$(LINUXHOME_4_9)/arch/arm/include/generated \
        -I./generated/arm/include \
        -I./generated/arm/arch/arm/include/generated \
        -include $(LINUXHOME_4_9)/include/linux/kconfig.h \
        -D__KERNEL__ \
        -D__LINUX_ARM_ARCH__=7 \
        -Uarm \
        -DCC_HAVE_ASM_GOTO \
        $(EXTRA_CPPFLAGS)

CFLAGS=\
      $(OPTFLAGS) \
      -g \
      -Wall \
      -mlittle-endian \
      -std=gnu89
endif
	
LDFLAGS=-static -Wl,-T,testbench.lds

HEADERS=at91sam9_wdt.h instrumentation/asm/io.h
SOURCES=at91sam9_wdt.c testbench.c
OBJ=$(SOURCES:.c=.o)
PREPROCESSED_SOURCES=$(SOURCES:.c=.i)
TESTBENCH=testbench

all: $(OBJ) $(PREPROCESSED_SOURCES) $(TESTBENCH)

clean:
	rm -f $(OBJ) $(PREPROCESSED_SOURCES) $(TESTBENCH)

at91sam9_wdt.o: at91sam9_wdt.c $(HEADERS)
	$(CROSS_COMPILE)gcc $(CPPFLAGS) $(CFLAGS) -DKBUILD_BASENAME='"at91sam9_wdt"' -DKBUILD_MODNAME='"at91sam9_wdt"' -c $< -o $@

testbench.o: testbench.c $(HEADERS)
	$(CROSS_COMPILE)gcc $(CPPFLAGS) $(CFLAGS) -DKBUILD_BASENAME='"testbench"' -DKBUILD_MODNAME='"testbench"' -c $< -o $@

at91sam9_wdt.i: at91sam9_wdt.c $(HEADERS)
	$(CROSS_COMPILE)gcc -E -dD $(CPPFLAGS) $(CFLAGS) -DKBUILD_BASENAME='"at91sam9_wdt"' -DKBUILD_MODNAME='"at91sam9_wdt"' $< -o $@

testbench.i: testbench.c $(HEADERS)
	$(CROSS_COMPILE)gcc -E -dD $(CPPFLAGS) $(CFLAGS) -DKBUILD_BASENAME='"testbench"' -DKBUILD_MODNAME='"testbench"' $< -o $@

testbench: $(OBJ)
	$(CROSS_COMPILE)gcc $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) $^ -o $@
